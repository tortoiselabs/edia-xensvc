#!/usr/bin/env python
"""
Copyright (c) 2012, 2013 TortoiseLabs LLC

All rights reserved.
"""

import os
import time
import subprocess
import select
import pyxen
import tempfile
from ediarpc import rpc_server
from pprint import pprint
import appliancekit.driver as akdrv

def domain_list():
    session = pyxen.Session()
    domlist = filter(lambda x: x.name is not None, session.domain_list())
    return {dom.name: dom.stats() for dom in domlist}

def domain_info(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)
    domain = session.find_domain(domname)
    return domain.stats()

def can_read_pty(pty):
    res = select.select([pty], [], [], 0)
    return res == ([pty], [], [])

def console_read(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)
    bytes = kwargs.pop('bytes', 64)

    domain = session.find_domain(domname)
    if domain is None:
        return {'error': 'Domain does not exist or is not running'}

    pty = open(domain.get_console_pty(), 'r')

    data = []
    while bytes:
        if can_read_pty(pty) is False:
            break

        data.append(pty.read(1))
        bytes -= 1

    pty.close()

    return {'console_data': ''.join(data)}

def console_write(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)
    bytes = kwargs.pop('bytes', None)

    if domname is None or bytes is None:
        return {'error': 'No domain or bytes provided to write'}

    domain = session.find_domain(domname)
    if domain is None:
        return {'error': 'Domain does not exist or is not running'}

    pty = open(domain.get_console_pty(), 'w')
    pty.write(bytes)
    pty.close()

    return {'written': bytes}

def shutdown(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)

    if domname is None:
        return {'error': 'No domain specified'}

    domain = session.find_domain(domname)
    if domain is None:
        return {'error': 'Domain does not exist or is not running'}

    domain.shutdown()

    return {'success': True}

def reboot(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)

    if domname is None:
        return {'error': 'No domain specified'}

    domain = session.find_domain(domname)
    if domain is None:
        return {'error': 'Domain does not exist or is not running'}

    domain.reboot()

    return {'success': True}

def destroy(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)

    if domname is None:
        return {'error': 'No domain specified'}

    domain = session.find_domain(domname)
    if domain is None:
        return {'error': 'Domain does not exist or is not running'}

#    domain.destroy()
    varlist = ['xl', 'destroy', domname]
    subprocess.call(varlist, close_fds=True)

    return {'success': True}

def pause(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)

    if domname is None:
        return {'error': 'No domain specified'}

    domain = session.find_domain(domname)
    if domain is None:
        return {'error': 'Domain does not exist or is not running'}

    domain.pause()

    return {'success': True}

def unpause(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)

    if domname is None:
        return {'error': 'No domain specified'}

    domain = session.find_domain(domname)
    if domain is None:
        return {'error': 'Domain does not exist or is not running'}

    domain.unpause()

    return {'success': True}

def create(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)
    mac = kwargs.pop('mac', '00:16:3e:00:00:01')
    hvm_nictype = kwargs.pop('hvm_nictype', 'e1000')

    if domname is None:
        return {'error': 'No domain specified'}

    cfg = "vcpus = 8\nname = '{0}'\n".format(domname)
    cfg += "disk = [ '/dev/vg0/{domname}-disk,raw,xvda1,rw', '/dev/vg0/{domname}-swap,raw,xvda2,rw' ]\n".format(domname=domname)

    for k, v in kwargs.items():
        if k == 'ips':
            cfg += "vif = ['ip={0},mac={1},model={2}']\n".format(' '.join(v), mac, hvm_nictype)
        elif k == 'disk' or k == 'device_model_args_hvm':
            cfg += "{0} = {1}\n".format(k, v)
        else:
            cfg += "{0} = '{1}'\n".format(k, v)

    print cfg

    cfg_fd = tempfile.NamedTemporaryFile()
    cfg_fd.write(cfg)
    cfg_fd.flush()
    varlist = ['xl', 'create', cfg_fd.name]
    res = subprocess.call(varlist, close_fds=True)
    cfg_fd.close()

    if res != 0 and res != -11:
        return {'error': 'xl create command failed: %d' % res}

    return {'success': True}

def confupdate(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)
    mac = kwargs.pop('mac', '00:16:3e:00:00:01')
    hvm_nictype = kwargs.pop('hvm_nictype', 'e1000')

    if domname is None:
        return {'error': 'No domain specified'}

    cfg = "vcpus = 8\nname='{0}'\n".format(domname)
    cfg += "disk = [ '/dev/vg0/{domname}-disk,raw,xvda1,rw', '/dev/vg0/{domname}-swap,raw,xvda2,rw' ]\n".format(domname=domname)

    for k, v in kwargs.items():
        if k == 'ips':
            cfg += "vif = ['ip={0},mac={1},model={2}']\n".format(' '.join(v), mac, hvm_nictype)
        elif k == 'disk' or k == 'device_model_args_hvm':
            cfg += "{0} = {1}\n".format(k, v)
        else:
            cfg += "{0}='{1}'\n".format(k, v)

    cfg_fd = tempfile.NamedTemporaryFile()
    cfg_fd.write(cfg)
    cfg_fd.flush()
    varlist = ['xl', 'config-updaate', domname, cfg_fd.name]
    res = subprocess.call(varlist, close_fds=True)
    cfg_fd.close()

    if res != 0 and res != -11:
        return {'error': 'xl config-update command failed: %d' % res}

    return {'success': True}

def schedupdate(**kwargs):
    session = pyxen.Session()
    domname = kwargs.pop('domname', None)
    weight = kwargs.pop('weight', None)
    pool = kwargs.pop('pool', None)

    if domname is None:
        return {'error': 'No domain specified'}

    if pool:
        varlist = ['xl', 'cpupool-migrate', domname, pool]
        res = subprocess.call(varlist, close_fds=True)

        if res != 0 and res != -11:
            return {'error': 'xl cpupool-migrate command failed: %d' % res}

    varlist = ['xl', 'sched-credit', '-d', domname, '-w', str(weight)]
    res = subprocess.call(varlist, close_fds=True)
    if res != 0 and res != -11:
        return {'error': 'xl sched-credit command failed: %d' % res}

    return {'success': True}

def vps_create(**kwargs):
    domname = kwargs.pop('domname', None)
    size = kwargs.pop('size', None)
    swap = kwargs.pop('swap', None)
    is_hvm = kwargs.pop('is_hvm', False)

    res = subprocess.call(['lvcreate', '-W', 'n', '-L', '{}G'.format(size), '-n', '{}-disk'.format(domname), 'vg0'], close_fds=True)
    if res != 0:
        return {'error': 'lvcreate disk failed'}

    if swap:
        subprocess.call(['lvcreate', '-W', 'n', '-L', '{}M'.format(swap), '-n', '{}-swap'.format(domname), 'vg0'], close_fds=True)
        subprocess.call(['mkswap', '/dev/vg0/{}-swap'.format(domname)], close_fds=True)

    # A stub config to make XL happy.
    cfg = """
name = "{domname}"
kernel = "/usr/lib/xen/boot/pv-grub-x86_64.gz"
extra = "(hd0)/boot/grub/menu.lst"
memory = 512
vcpus = 8
vif = []
disk = [ '/dev/vg0/{domname}-disk,raw,xvda1,rw', '/dev/vg0/{domname}-swap,raw,xvda2,rw' ]
    """.format(domname=domname)
    cfg_fd = open('/etc/xen/{}.cfg'.format(domname), 'w')
    cfg_fd.write(cfg)
    cfg_fd.close()

    return {'success': True}

def vps_destroy(**kwargs):
    domname = kwargs.pop('domname', None)
    subprocess.call(['xl', 'destroy', domname], close_fds=True)
    subprocess.call(['lvremove', '--force', '/dev/vg0/{}-disk'.format(domname)], close_fds=True)
    subprocess.call(['lvremove', '--force', '/dev/vg0/{}-swap'.format(domname)], close_fds=True)
    os.unlink('/etc/xen/{}.cfg'.format(domname))
    return {'success': True}

def vps_format(**kwargs):
    domname = kwargs.pop('domname', None)
    subprocess.call(['xl', 'destroy', domname], close_fds=True)
    subprocess.call(['mke2fs', '-j', '/dev/vg0/{}-disk'.format(domname)], close_fds=True)
    return {'success': True}

def vps_image(**kwargs):
    domname = kwargs.pop('domname', None)
    image = kwargs.pop('image', None)
    arch = kwargs.pop('arch', 'x86_64')

    if not domname or not image:
        return {'error': 'domname or image name missing'}

    tmpdir = tempfile.mkdtemp(prefix='img_vps.')
    subprocess.call(['mount', '-o', 'async,data=writeback,barrier=0,nobh,relatime', '/dev/vg0/{}-disk'.format(domname), tmpdir], close_fds=True)

    akdrv.run_axml('/etc/appliancekit/templates/' + image, tmpdir, kwargs, arch=arch)

    subprocess.call(['umount', tmpdir])
    os.rmdir(tmpdir)

    return {'success': True}

def vps_rootpass(**kwargs):
    domname = kwargs.pop('domname', None)
    newpasshash = kwargs.pop('newpasshash', None)

    if not domname or not newpasshash:
        return {'error': 'domname or new password hash missing'}

    tmpdir = tempfile.mkdtemp(prefix='img_vps.')
    subprocess.call(['mount', '/dev/vg0/{}-disk'.format(domname), tmpdir], close_fds=True)

    proc = subprocess.Popen(['chroot', tmpdir, 'chpasswd', '-e'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)
    proc.communicate('root:{}\n'.format(newpasshash))
    proc.stdin.close()
    if proc.wait() != 0:
        return {'error': 'proc.wait() returned non-zero'}

    subprocess.call(['umount', tmpdir])
    os.rmdir(tmpdir)

    return {'success': True}

def vps_clone(**kwargs):
    domname = kwargs.pop('domname', None)
    image = kwargs.pop('image', 'noconfig')
    targetip = kwargs.pop('targetip', None)
    arch = kwargs.pop('arch', 'x86_64')

    if not domname or not targetip:
        return {'error': 'domname or new password hash missing'}

    try:
        st = os.stat('/root/.ssh/tmp_rsa')
        if time.time() - st.st_mtime > 3600:
            pubkey = open('/root/.ssh/tmp_rsa.pub').read().strip()
            return {'error': 'No valid keypair is available'}
    except OSError:
        return {'error': 'No valid keypair is available'}
    
    tmpdir = tempfile.mkdtemp(prefix='img_vps.')
    subprocess.call(['mount', '/dev/vg0/{}-disk'.format(domname), tmpdir], close_fds=True)

    subprocess.call(['rsync', '-e', 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i /root/.ssh/tmp_rsa', '-avzHAX', '--exclude', '/proc/*', '--exclude', '/sys/*', 'root@{}:/'.format(targetip), '/{}/'.format(tmpdir)])
    if image != 'noconfig':
        akdrv.run_axml('/etc/appliancekit/templates/' + image, tmpdir, kwargs, arch=arch, phases=['configure', 'xen-tweaks', 'cleanup'])

    subprocess.call(['umount', tmpdir])
    os.rmdir(tmpdir)

    return {'success': True}    

def vps_migrate(**kwargs):
    domname = kwargs.pop('domname', None)
    targetip = kwargs.pop('targetip', None)

    if not domname or not targetip:
        return {'error': 'domname or target ip missing'}

    tmpdir = tempfile.mkdtemp(prefix='img_vps.')
    subprocess.call(['mount', '/dev/vg0/{}-disk'.format(domname), tmpdir], close_fds=True)
    subprocess.call(['rsync', '-e', 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no', '-avzHAX', '--numeric-ids', 'root@{}:/mnt/'.format(targetip), '/{}/'.format(tmpdir)])
    subprocess.call(['umount', tmpdir])
    os.rmdir(tmpdir)

    return {'success': True}

def vps_mount(**kwargs):
    domname = kwargs.pop('domname', None)
    endpoint = kwargs.pop('endpoint', None)

    if not domname or not endpoint:
        return {'error': 'domname or endpoint missing'}

    subprocess.call(['mount', '/dev/vg0/{}-disk'.format(domname), endpoint], close_fds=True)

    return {'success': True}

def vps_umount(**kwargs):
    domname = kwargs.pop('domname', None)
    endpoint = kwargs.pop('endpoint', None)

    if not domname or not endpoint:
        return {'error': 'domname or endpoint missing'}

    subprocess.call(['umount', endpoint], close_fds=True)

    return {'success': True}

def tmp_keypair_gen(**kwargs):
    try:
        st = os.stat('/root/.ssh/tmp_rsa')
        if time.time() - st.st_mtime < 3600:
            pubkey = open('/root/.ssh/tmp_rsa.pub').read().strip()
            return {'pubkey': pubkey}
    except OSError:
        pass

    try:
        os.unlink('/root/.ssh/tmp_rsa')
        os.unlink('/root/.ssh/tmp_rsa.pub')
    except OSError:
        pass

    subprocess.call(['ssh-keygen', '-f', '/root/.ssh/tmp_rsa', '-P', '', '-t', 'rsa'])
    pubkey = open('/root/.ssh/tmp_rsa.pub').read().strip()
    return {'pubkey': pubkey}

def main():
    cfg = open('/etc/edia.passphrase', 'r')
    key = cfg.readline().strip()
    iterations = int(cfg.readline().strip())
    cfg.close()

    srv = rpc_server.RPCServer(('0.0.0.0', 5959), key, iterations=iterations)

    srv.bind(domain_list)
    srv.bind(domain_info)
    srv.bind(console_read)
    srv.bind(console_write)

    srv.bind(create)
    srv.bind(confupdate)
    srv.bind(schedupdate)
    srv.bind(shutdown)
    srv.bind(reboot)
    srv.bind(destroy)
    srv.bind(pause)
    srv.bind(unpause)

    srv.bind(vps_create)
    srv.bind(vps_destroy)
    srv.bind(vps_format)
    srv.bind(vps_image)
    srv.bind(vps_rootpass)
    srv.bind(vps_clone)
    srv.bind(vps_migrate)
    srv.bind(vps_mount)
    srv.bind(vps_umount)

    srv.bind(tmp_keypair_gen)

    srv.serve_forever()

if __name__ == '__main__':
    main()
